package messaging;

/**
 * Created by Lisa on 8/09/2016.
 */
public class ActiveFileSetUpdateRequestMessage {

    private String action;
    private int projectId;
    private int userId;
    private int fileId;

    public ActiveFileSetUpdateRequestMessage(String action, int projectId, int userId, int fileId) {
        this.action = action;
        this.projectId = projectId;
        this.userId = userId;
        this.fileId = fileId;
    }

    public ActiveFileSetUpdateRequestMessage(String action, int projectId, int userId) {
        this.action = action;
        this.projectId = projectId;
        this.userId = userId;
    }

}
