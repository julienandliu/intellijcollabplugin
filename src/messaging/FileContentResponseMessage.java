package messaging;

/**
 * Created by Lisa on 8/09/2016.
 */
public class FileContentResponseMessage {

    private String requestID;
    private int responseCode;
    private int fileId;
    private String content;
    private int revision;

    public FileContentResponseMessage(String requestID, int responseCode, int fileId, String content, int revision) {
        this.requestID = requestID;
        this.responseCode = responseCode;
        this.fileId = fileId;
        this.content = content;
        this.revision = revision;

    }

    public String getRequestID() {
        return requestID;
    }

    public String getFileContent() {
        return content;
    }

    public int getRevision() {
        return revision;
    }


}
