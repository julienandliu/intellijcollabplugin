package messaging;

/**
 * Created by Lisa on 8/09/2016.
 */
public class FileContentRequestMessage {

    private String action;
    private String requestID;
    private int userId;
    private int projectId;
    private int fileId;

    public FileContentRequestMessage(String action, String requestID, int userId, int projectId, int fileId) {
        this.action = action;
        this.requestID = requestID;
        this.userId = userId;
        this.projectId = projectId;
        this.fileId = fileId;
    }

}
