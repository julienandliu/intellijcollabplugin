package handlers;

import collab.Activator;
import collab.FTPUtil;
import collab.IntellijCollabSingleton;
import com.intellij.ide.util.projectWizard.JavaModuleBuilder;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.ModifiableModuleModel;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ex.ProjectManagerEx;
import com.intellij.openapi.project.impl.ProjectManagerImpl;
import com.intellij.openapi.projectRoots.ProjectJdkTable;
import com.intellij.openapi.projectRoots.Sdk;
import com.intellij.openapi.roots.CompilerModuleExtension;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.ex.ProjectRootManagerEx;
import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.MultiLineLabelUI;
import com.intellij.openapi.util.Pair;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.net.ftp.FTPSClient;
import org.bson.Document;
import ui.LoginDialog;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lisa on 9/09/2016.
 */
public class ConnectToProject extends AnAction {

    private Activator activator;
    private IntellijCollabSingleton main;
    private String server = "52.203.246.100";
    private String user = "hello";
    private String pass = "world";
    private FTPSClient ftpClient = new FTPSClient(true);

    /**
     * This method is invoked when "Connect to Collaborative Project" is clicked
     * on the welcome screen. This assumes that the project has never previously
     * been connected before
     *
     * @param e
     */
    @Override
    public void actionPerformed(AnActionEvent e) {

        Project project = e.getData(PlatformDataKeys.PROJECT);
        DialogBuilder builder = new DialogBuilder();
        builder.setTitle("Intellij Collaboration Plugin");
        builder.addOkAction();
        builder.addCancelAction();

        DialogBuilder info = new DialogBuilder();
        info.setTitle("Intellij Collaboration Plugin");
        info.addOkAction();

        if (main == null) {
            activator = ApplicationManager.getApplication().getComponent(Activator.class);
            main = activator.getSingleton();
        }

        if (!main.isLoggedIn()) {
            String[] credentials = LoginDialog.main();

            String username = credentials[0];
            String password = credentials[1];

            main.setUserCredentials(username, password);

            while (!main.validateCredentials()) {

                JLabel label = new JLabel("Problem with account details, try again");
                label.setUI(new MultiLineLabelUI());
                builder.setCenterPanel(label);

                switch (builder.show()) {
                    case DialogWrapper.OK_EXIT_CODE:
                        credentials = LoginDialog.main();
                        username = credentials[0];
                        password = credentials[1];

                        if (username == null && password == null) {
                            return;
                        }

                        main.setUserCredentials(username, password);

                        break;
                    case DialogWrapper.CANCEL_EXIT_CODE:
                        return;
                    default:
                        break;
                }
            }
        }


        String projectName = Messages.showInputDialog(project,
                "Enter name of project to connect to", "Connect to Existing Project", null);

        if (projectName != null) {
            String projectID = getProjectID(projectName);
            main.putProjectNameToIDMapping(projectName, Integer.parseInt(projectID));

            String saveDirPath = com.intellij.ide.impl.ProjectUtil.getBaseDir();
            String projectPath = saveDirPath + "/" + projectID;
            String projectPathName = saveDirPath + "/" + projectName;

            // The project will be renamed later on so save the path with projectName
            main.setConnectedProject(projectName, projectPathName);

            JLabel label;

            if (projectID != null) {

                // Get the project location
                if (downloadProject(projectID, projectName, saveDirPath, projectPath)) {
                    main.putProjectNameToIDMapping(projectName, Integer.parseInt(projectID));
                    System.out.println("Project downloaded ok");
                } else {

                    label = new JLabel("Problem connecting to the project");
                    label.setUI(new MultiLineLabelUI());
                    info.setCenterPanel(label);
                    info.show();

                }
            } else {

                label = new JLabel("Problem connecting to the project");
                label.setUI(new MultiLineLabelUI());
                info.setCenterPanel(label);
                info.show();

            }

        }

        return;
    }

    /**
     * Return the project name so it can be accessed from the server's
     * eclipse workspace
     *
     * @param projectName
     * @return
     */
    private String getProjectID(String projectName) {

        MongoClient mongoClient = main.getMongoClient();
        MongoDatabase db = mongoClient.getDatabase("testdb");

        int userID = main.getUserID();

        List<BasicDBObject> or = new ArrayList<BasicDBObject>();
        BasicDBObject orQuery = new BasicDBObject();
        or.add(new BasicDBObject("collaborators", userID));
        or.add(new BasicDBObject("ownerId", userID));
        orQuery.put("$or", or);

        List<BasicDBObject> and = new ArrayList<BasicDBObject>();
        BasicDBObject andQuery = new BasicDBObject();
        and.add(orQuery);
        and.add(new BasicDBObject("projectName", projectName));
        andQuery.put("$and", and);

        Integer projectID = null;
        FindIterable<Document> iterable = db.getCollection("projects").find(andQuery);

        if (!iterable.iterator().hasNext()) {
            System.out.println("User is not the owner or collaborator of a project by the given name");
            return null;
        }

        for (Document current : iterable) {
            projectID = current.getInteger("_id");
        }

        return Integer.toString(projectID);
    }

    private Project createProject(String projectName, String projectFilePath) {

        Project project = ProjectManagerImpl.getInstanceEx().newProject(
                projectName, projectFilePath, true, false);

        Sdk[] jdks = ProjectJdkTable.getInstance().getAllJdks();
        final Sdk projectjdk = jdks[0];
        if (projectjdk != null) {
            ApplicationManager.getApplication().runWriteAction(new Runnable() {
                public void run() {
                    ProjectRootManagerEx.getInstanceEx(project).setProjectSdk(projectjdk);
                }
            });
        }


        project.save();
        return project;

    }

    /**
     * Use FTP Client example to download the project to the local workspace. Note
     * that there is a bug with Intellij. The project downloads, imports and opens
     * okay, however before you run the project, you must go to to
     * File -> Module -> Paths and then click Apply or OK, and the output will
     * go the to Bin folder. Will open an issue/ticket with Intellij at some stage.
     *
     * @param projectID
     * @param projectName
     * @return
     */
    private boolean downloadProject(String projectID, String projectName, String saveDirPath, String projectPath) {

        try {
            // connect and login to the server
            ftpClient.connect(server);
            ftpClient.login(user, pass);

            // Set protection buffer size
            ftpClient.execPBSZ(0);
            // Set data channel protection to private
            ftpClient.execPROT("P");

            // use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

            System.out.println("Connected");

            String remoteDirPath = "workspace";


            String downloadedPath = saveDirPath + "/" + projectName;

            // Download the project
            FTPUtil.downloadDirectory(ftpClient, remoteDirPath, projectID, saveDirPath, true, projectName);

            // Create a new project
            Project newProject = createProject(projectName, downloadedPath);

            //code to create module and add it to project
            String outputPath = downloadedPath + File.separator + "bin";
            JavaModuleBuilder moduleBuilder = new JavaModuleBuilder();
            ModifiableModuleModel model =
                    ModuleManager.getInstance(newProject).getModifiableModel();


            moduleBuilder.setName(projectName);

            String newProjectImplFilePath = downloadedPath + File.separator + ".iml";
            moduleBuilder.setModuleFilePath(newProjectImplFilePath);
            moduleBuilder.setContentEntryPath(downloadedPath);
            Sdk[] sdks = ProjectJdkTable.getInstance().getAllJdks();
            final Sdk projectjdkk = sdks[0];

            String srcPath = downloadedPath + File.separator + "src";


            if (model != null) {
                ApplicationManager.getApplication().runWriteAction(new Runnable() {
                    public void run() {
                        try {
                            Module newModule = moduleBuilder.createModule(model);
                            model.renameModule(newModule, projectName);
                            moduleBuilder.setModuleJdk(projectjdkk);
                            moduleBuilder.addSourcePath(new Pair(srcPath, ""));
                            moduleBuilder.setCompilerOutputPath(outputPath);


                            ModifiableRootModel rootModel =
                                    ModuleRootManager.getInstance(newModule).getModifiableModel();

                            final CompilerModuleExtension extension =
                                    rootModel.getModuleExtension(CompilerModuleExtension.class);
                            extension.setCompilerOutputPath(outputPath);
                            extension.inheritCompilerOutputPath(false);
                            extension.setExcludeOutput(true);

                            moduleBuilder.commit(newProject);
                            extension.commit();
                            model.commit();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            newProject.save();

            ProjectManagerEx.getInstanceEx().openProject(newProject);

            // log out and disconnect from the server
            ftpClient.logout();
            ftpClient.disconnect();

            System.out.println("Disconnected");
            return true;

        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }

    }
}
