package handlers;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;

/**
 * Created by Lisa on 10/09/2016.
 */
public class UploadProjectToCloud  extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {

        PropertiesComponent properties = PropertiesComponent.getInstance();

        // TODO: insert action logic here
        Project project = e.getData(PlatformDataKeys.PROJECT);
        String txt= Messages.showInputDialog(project, "Enter the URL to your profile", "Connect to Web based IDE", Messages.getQuestionIcon());
        Messages.showMessageDialog(String.valueOf(project), "Coming soon...", Messages.getInformationIcon());


    }
}
