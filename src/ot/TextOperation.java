package ot;

import com.eclipsesource.json.JsonArray;

import java.util.ArrayList;

/**
 * Created by Lisa on 15/09/2016.
 */

public class TextOperation {

    private ArrayList<Object> ops;
    private int baseLength = 0;
    private int targetLength = 0;

    public TextOperation() {
        ops = new ArrayList<>();
    }

    public boolean isRetain(Object op) {
        if (op == null) {
            return false;
        }

        if (op instanceof String) {
            return false;
        }

        int opInt = (int) op;
        return opInt > 0;
    }

    public boolean isInsert(Object op) {
        if (op == null) {
            return false;
        }

        return op instanceof String;
    }

    public boolean isDelete(Object op) {
        if (op == null) {
            return false;
        }

        if (op instanceof String) {
            return false;
        }

        int opInt = (int) op;
        return opInt < 0;
    }

    public TextOperation retain(Object op) {
        int n;
        try {
            n = (int) op;
        } catch (ClassCastException cce) {
            System.out.println("SOULD NEVER GET HERE - retain should get an int, not a string");
            return null;
        } catch (NullPointerException npe) {
            System.out.println("SOULD NEVER GET HERE - retain should get an int, not a null");
            return null;
        }

        if (n == 0) {
            return this;
        }
        this.baseLength += n;
        this.targetLength += n;

        if (this.ops.size() == 0) {
            this.ops.add(n);
            return this;
        }

        Object lastOp = this.ops.get(this.ops.size() - 1);
        if (isRetain(lastOp)) {
            int lastOpInt = (int) lastOp;
            lastOpInt += n;
            this.ops.set(this.ops.size() - 1, lastOpInt);
        } else {
            this.ops.add(n);
        }

        return this;
    }

    public TextOperation insert(Object op) {
        String str;
        try {
            str = (String) op;
        } catch (ClassCastException cce) {
            System.out.println("SOULD NEVER GET HERE - insert should get a string");
            return null;
        } catch (NullPointerException npe) {
            System.out.println("SOULD NEVER GET HERE - insert shouldnt get a null");
            return null;
        }

        if (str == "") {
            return this;
        }
        this.targetLength += str.length();

        if (this.ops.size() == 0) {
            this.ops.add(str);
            return this;
        }

        Object lastOp = this.ops.get(this.ops.size() - 1);
        if (isInsert(lastOp)) {
            String lastOpString = (String) lastOp;
            lastOpString += str;
            this.ops.set(this.ops.size() - 1, lastOpString);
        } else if (isDelete(lastOp)) {
            if (this.ops.size() == 1) {
                this.ops.add(lastOp);
                this.ops.set(this.ops.size() - 2, str);
                return this;
            }
            Object beforeLastOp = this.ops.get(this.ops.size() - 2);
            if (isInsert(beforeLastOp)) {
                String beforeLastOpString = (String) beforeLastOp;
                beforeLastOpString += str;
                this.ops.set(this.ops.size() - 2, beforeLastOpString);
            } else {
                this.ops.add(lastOp);
                this.ops.set(this.ops.size() - 2, str);
            }
        } else {
            this.ops.add(str);
        }

        return this;
    }

    public TextOperation delete(Object op) {
        int n;

        if (op == null) {
            System.out.println("SOULD NEVER GET HERE - delete shouldnt get a null");
            return null;
        }

        if (op instanceof String) {
            n = ((String) op).length();
        } else {
            try {
                n = (int) op;
            } catch (ClassCastException cce) {
                System.out.println("SOULD NEVER GET HERE - delete should get a string or an int");
                return null;
            }
        }

        if (n == 0) {
            return this;
        }
        if (n > 0) {
            n = -n;
        }
        this.baseLength -= n;

        if (this.ops.size() == 0) {
            this.ops.add(n);
            return this;
        }

        Object lastOp = this.ops.get(this.ops.size() - 1);
        if (isDelete(lastOp)) {
            int lastOpInt = (int) lastOp;
            lastOpInt += n;
            this.ops.set(this.ops.size() - 1, lastOpInt);
        } else {
            this.ops.add(n);
        }

        return this;
    }

    public JsonArray toJson() {
        JsonArray jsonArray = new JsonArray();
        for (Object op : ops) {
            if (op instanceof String) {
                jsonArray.add((String) op);
            } else {
                jsonArray.add((int) op);
            }
        }
        return jsonArray;
    }

    public void fromJson(JsonArray array) {
        for (int i = 0; i < array.size(); i++) {

            Object op;
            if (array.get(i).isNumber()) {
                op = (Object) array.get(i).asInt();
            } else {
                op = (Object) array.get(i).asString();
            }

            if (isRetain(op)) {
                this.retain(op);
            } else if (isInsert(op)) {
                this.insert(op);
            } else if (isDelete(op)) {
                this.delete(op);
            } else {
                System.out.println("SHOULD NOT GET HERE - Unknown operation");
            }
        }
    }

    public String apply(String str) {

        if (str.length() != this.getBaseLength()) {
            System.out.println("SHOULD NEVER GET HERE - "
                    + "The operation's base length must be equal to the string's length.");
        }

        String newStr = "";
        int strIndex = 0;

        for (int i = 0, l = this.ops.size(); i < l; i++) {
            Object op = this.ops.get(i);

            if (isRetain(op)) {

                int opRetain = (int) op;
                if (strIndex + opRetain > str.length()) {
                    System.out.println("SHOULD NEVER GET HERE - "
                            + "Operation can't retain more characters than are left in the string.");
                }
                // Copy skipped part of the old string.
                newStr += str.substring(strIndex, strIndex + opRetain);
                strIndex += opRetain;

            } else if (isInsert(op)) {

                String opInsert = (String) op;
                // Insert string.
                newStr += opInsert;

            } else { // delete op

                int opDelete = (int) op;
                strIndex -= opDelete;

            }
        }

        if (strIndex != str.length()) {
            System.out.println("SHOULD NEVER GET HERE - The operation didn't operate on the whole string.");
        }
        return newStr;
    }

    public TextOperation compose(TextOperation operation2) {

        if (this.targetLength != operation2.getBaseLength()) {
            System.out.println("SHOULD NEVER GET HERE - "
                    + "The base length of the second operation has to be the target length of the first operation");
        }

        TextOperation operation = new TextOperation(); // the combined operation

        ArrayList<Object> ops1 = this.ops;
        ArrayList<Object> ops2 = operation2.getOps();

        System.out.println("COMPOSE START --- ops1: " + ops1.toString());
        System.out.println("--- ops2: " + ops2.toString());

        int i1 = 0;
        int i2 = 0;
        Object op1;
        if (i1 == ops1.size()) {
            op1 = null;
        } else {
            op1 = ops1.get(i1++);
        }
        Object op2;
        if (i2 == ops2.size()) {
            op2 = null;
        } else {
            op2 = ops2.get(i2++);
        }

        while (true) {

            System.out.println("ENTERING LOOP -- operation: " + operation.toString());
            if (op1 == null) {
                System.out.println(" -- op1: null");
            } else {
                System.out.println(" -- op1: " + op1.toString());
            }
            if (op2 == null) {
                System.out.println(" -- op2: null");
            } else {
                System.out.println(" -- op2: " + op2.toString());
            }
            System.out.println(" -- i1: " + i1 + ", i2: " + i2);

            // Dispatch on the type of op1 and op2
            if (op1 == null && op2 == null) {
                // end condition: both ops1 and ops2 have been processed
                break;
            }

            if (isDelete(op1)) {
                operation.delete(op1);
                if (i1 == ops1.size()) {
                    op1 = null;
                } else {
                    op1 = ops1.get(i1++);
                }
                continue;
            }
            if (isInsert(op2)) {
                operation.insert(op2);
                if (i2 == ops2.size()) {
                    op2 = null;
                } else {
                    op2 = ops2.get(i2++);
                }
                continue;
            }

            if (op1 == null) {
                System.out.println("SHOULD NEVER GET HERE - "
                        + "Cannot compose operations: first operation is too short.");
            }
            if (op2 == null) {
                System.out.println("SHOULD NEVER GET HERE - "
                        + "Cannot compose operations: first operation is too long.");
            }

            int op1Int;
            int op2Int;
            String op1String;

            if (isRetain(op1) && isRetain(op2)) {
                op1Int = (int) op1;
                op2Int = (int) op2;

                if (op1Int > op2Int) {
                    operation.retain(op2);
                    op1 = op1Int - op2Int;
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1Int == op2Int) {
                    operation.retain(op1);
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    operation.retain(op1);
                    op2 = op2Int - op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

            } else if (isInsert(op1) && isDelete(op2)) {
                op1String = (String) op1;
                op2Int = (int) op2;

                if (op1String.length() > -op2Int) {
                    op1 = op1String.substring(-op2Int);
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1String.length() == -op2Int) {
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    op2 = op2Int + op1String.length();
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

            } else if (isInsert(op1) && isRetain(op2)) {
                op1String = (String) op1;
                op2Int = (int) op2;

                if (op1String.length() > op2Int) {
                    operation.insert(op1String.substring(0, op2Int));
                    op1 = op1String.substring(op2Int);
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1String.length() == op2Int) {
                    operation.insert(op1);
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    operation.insert(op1);
                    op2 = op2Int - op1String.length();
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

            } else if (isRetain(op1) && isDelete(op2)) {
                op1Int = (int) op1;
                op2Int = (int) op2;

                if (op1Int > -op2Int) {
                    operation.delete(op2);
                    op1 = op1Int + op2Int;
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1Int == -op2Int) {
                    operation.delete(op2);
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    operation.delete(op1);
                    op2 = op2Int + op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }
            } else {
                System.out.println("SHOULD NEVER GET HERE - "
                        + "This shouldn't happen: op1: " + op1.toString() + ", op2: " + op2.toString());
            }
        }

        return operation;
    }

    // ---------------- Static method access -------------- //

    public static ArrayList<TextOperation> transform(TextOperation operation1, TextOperation operation2) {
        TextOperation operation = new TextOperation();
        return operation.transformImpl(operation1, operation2);
    }

    // ---------------- Private methods ------------------ //

    private ArrayList<TextOperation> transformImpl(TextOperation operation1, TextOperation operation2) {

        if (operation1.getBaseLength() != operation2.getBaseLength()) {
            System.out.println("SHOULD NEVER GET HERE - Both operations have to have the same base length");
        }

        TextOperation operation1prime = new TextOperation();
        TextOperation operation2prime = new TextOperation();
        ArrayList<Object> ops1 = operation1.getOps();
        ArrayList<Object> ops2 = operation2.getOps();

        int i1 = 0;
        int i2 = 0;
        Object op1;
        if (i1 == ops1.size()) {
            op1 = null;
        } else {
            op1 = ops1.get(i1++);
        }
        Object op2;
        if (i2 == ops2.size()) {
            op2 = null;
        } else {
            op2 = ops2.get(i2++);
        }

        while (true) {
            // At every iteration of the loop, the imaginary cursor that both
            // operation1 and operation2 have that operates on the input string must
            // have the same position in the input string.

            if (op1 == null && op2 == null) {
                // end condition: both ops1 and ops2 have been processed
                break;
            }

            // next two cases: one or both ops are insert ops
            // => insert the string in the corresponding prime operation, skip it in
            // the other one. If both op1 and op2 are insert ops, prefer op1.
            if (isInsert(op1)) {
                operation1prime.insert(op1);
                operation2prime.retain(((String) op1).length());
                if (i1 == ops1.size()) {
                    op1 = null;
                } else {
                    op1 = ops1.get(i1++);
                }
                continue;
            }
            if (isInsert(op2)) {
                operation1prime.retain(((String) op2).length());
                operation2prime.insert(op2);

                if (i2 == ops2.size()) {
                    op2 = null;
                } else {
                    op2 = ops2.get(i2++);
                }
                continue;
            }

            if (op1 == null) {
                System.out.println("SHOULD NEVER GET HERE - "
                        + "Cannot compose operations: first operation is too short.");
            }
            if (op2 == null) {
                System.out.println("SHOULD NEVER GET HERE - "
                        + "Cannot compose operations: first operation is too long.");
            }

            int minl;
            int op1Int = (int) op1;
            int op2Int = (int) op2;

            if (isRetain(op1) && isRetain(op2)) {
                // Simple case: retain/retain

                if (op1Int > op2Int) {
                    minl = op2Int;
                    op1 = op1Int - op2Int;
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1Int == op2Int) {
                    minl = op2Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    minl = op1Int;
                    op2 = op2Int - op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

                operation1prime.retain(minl);
                operation2prime.retain(minl);

            } else if (isDelete(op1) && isDelete(op2)) {
                // Both operations delete the same string at the same position. We don't
                // need to produce any operations, we just skip over the delete ops and
                // handle the case that one operation deletes more than the other.

                if (-op1Int > -op2Int) {
                    op1 = op1Int - op2Int;
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1Int == op2Int) {
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    op2 = op2Int - op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

                // next two cases: delete/retain and retain/delete
            } else if (isDelete(op1) && isRetain(op2)) {

                if (-op1Int > op2Int) {
                    minl = op2Int;
                    op1 = op1Int + op2Int;
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (-op1Int == op2Int) {
                    minl = op2Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    minl = -op1Int;
                    op2 = op2Int + op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

                operation1prime.delete(minl);

            } else if (isRetain(op1) && isDelete(op2)) {

                if (op1Int > -op2Int) {
                    minl = -op2Int;
                    op1 = op1Int + op2Int;
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else if (op1Int == -op2Int) {
                    minl = op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                    if (i2 == ops2.size()) {
                        op2 = null;
                    } else {
                        op2 = ops2.get(i2++);
                    }
                } else {
                    minl = op1Int;
                    op2 = op2Int + op1Int;
                    if (i1 == ops1.size()) {
                        op1 = null;
                    } else {
                        op1 = ops1.get(i1++);
                    }
                }

                operation2prime.delete(minl);

            } else {
                System.out.println("SHOULD NEVER GET HERE - The two operations aren't compatible");
            }
        }

        ArrayList<TextOperation> result = new ArrayList<>();
        result.add(operation1prime);
        result.add(operation2prime);

        return result;
    }

    // --------------- Getters and setters --------------- //

    public ArrayList<Object> getOps() {
        return ops;
    }

    public void setOps(ArrayList<Object> ops) {
        this.ops = ops;
    }

    public int getBaseLength() {
        return baseLength;
    }

    public void setBaseLength(int baseLength) {
        this.baseLength = baseLength;
    }

    public int getTargetLength() {
        return targetLength;
    }

    public void setTargetLength(int targetLength) {
        this.targetLength = targetLength;
    }


    // ------------ Override -------------------------- //

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof TextOperation)) {
            return false;
        }

        TextOperation other = (TextOperation) o;

        if (this.baseLength != other.getBaseLength()) {
            return false;
        }
        if (this.targetLength != other.getTargetLength()) {
            return false;
        }
        if (this.ops.size() != other.getOps().size()) {
            return false;
        }

        ArrayList<Object> otherOps = other.getOps();

        for (int i = 0; i < this.ops.size(); i++) {
            Object op = this.ops.get(i);
            Object otherOp = otherOps.get(i);

            if (op instanceof String && !(otherOp instanceof String)) {
                return false;
            } else if (!(op instanceof String) && otherOp instanceof String) {
                return false;
            } else if (op instanceof String && otherOp instanceof String) {
                String opText = (String) op;
                String otherOpText = (String) otherOp;

                if (!opText.equals(otherOpText)) {
                    return false;
                }
            } else { // both int
                int opInt = (int) op;
                int otherOpInt = (int) otherOp;

                if (opInt != otherOpInt) {
                    return false;
                }
            }
        }
        return true;

    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + this.baseLength;
        result = 31 * result + this.targetLength;
        result = 31 * result + this.ops.size();

        for (int i = 0; i < this.ops.size(); i++) {
            Object op = this.ops.get(i);
            if (op instanceof String) {
                result = 31 * result + ((String) op).hashCode();
            } else {
                result = 31 * result + ((int) op);
            }

        }

        return result;
    }

    @Override
    public String toString() {
        String str = "";

        for (Object op : ops) {
            if (isRetain(op)) {
                int retainOp = (int) op;
                str += "retain " + retainOp;
            } else if (isInsert(op)) {
                String insertOp = (String) op;
                str += "insert '" + insertOp + "'";
            } else {
                int deleteOp = (int) op;
                str += "delete " + op;
            }
        }

        return str;
    }
}


