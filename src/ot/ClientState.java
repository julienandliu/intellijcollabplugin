package ot;

/**
 * Created by Lisa on 15/09/2016.
 */
public interface ClientState {

    public ClientState applyClient(ClientManager client, TextOperation operation);

    public ClientState applyServer(ClientManager client, TextOperation operation);

    public ClientState serverAck(ClientManager client);

}

