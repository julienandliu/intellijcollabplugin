package ot;

/**
 * Created by Lisa on 15/09/2016.
 */
public class ClientSynchronized implements ClientState {

    public ClientSynchronized() {

    }

    @Override
    public ClientState applyClient(ClientManager client, TextOperation operation) {
        client.sendOperation(client.getRevision(), operation);
        return new ClientAwaitingConfirm(operation);
    }

    @Override
    public ClientState applyServer(ClientManager client, TextOperation operation) {
        client.applyOperation(client.getFileId(), operation);
        return this;
    }

    @Override
    public ClientState serverAck(ClientManager client) {
        System.out.println("SHOULD NEVER GET HERE - serverAck should not be called on synchronized state");
        return null;
    }

}
