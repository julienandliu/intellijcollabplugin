package ot;

import collab.Activator;
import collab.IDEManager;
import collab.IntellijCollabSingleton;
import com.eclipsesource.json.JsonObject;
import com.intellij.openapi.application.ApplicationManager;

/**
 * Created by Lisa on 15/09/2016.
 */
public class ClientManager {

    private IntellijCollabSingleton main;

    private int fileId;
    private int revision;
    private ClientState state;


    public ClientManager(int fileId, int revision) {
        this.fileId = fileId;
        this.revision = revision;
        this.state = new ClientSynchronized();
        main = ApplicationManager.getApplication().getComponent(Activator.class).getSingleton();
    }

    // Call this method when the user changes the document.
    public void applyClient(TextOperation operation) {
        this.setState(this.state.applyClient(this, operation));
    }

    // Call this method with a new operation from the server
    public void applyServer(TextOperation operation) {
        this.revision++;
        this.setState(this.state.applyServer(this, operation));
    }

    // Call this method when receiving an operation initiated by this user
    public void serverAck() {
        this.revision++;
        this.setState(this.state.serverAck(this));
    }

    public void sendOperation(int revision, TextOperation operation) {

        JsonObject op = new JsonObject();
        op.add("action", "operation");
        op.add("userId", main.getUserID());
        op.add("projectId", main.getCurrentlyConnectedProject().getProjectID());
        op.add("fileId", main.getCurrentlyOpenFileId());
        op.add("revision", revision);
        op.add("operation", operation.toJson());

        System.out.println("Sending operation: " + operation.toString());

        if (main.getRealTimeWS().isConnected()) {
            main.getRealTimeWS().sendMessage(op.toString());
        }
    }

    public void applyOperation(int fileId, TextOperation operation) {
        IDEManager ide =  ApplicationManager.getApplication().getComponent(Activator.class).getIDEManager();

        if (fileId == main.getCurrentlyOpenFileId()) {
            ide.insertChangesInEditor(operation);
        }

        // Update the document
        String doc = ide.getContentForFileId(fileId);
        String updatedDoc = operation.apply(doc);
        ide.storeContentForFileId(fileId, updatedDoc);
    }

    // -------------------- Getters and setters -----------------------------//

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(int revision) {
        this.revision = revision;
    }

    public ClientState getState() {
        return state;
    }

    public void setState(ClientState state) {
        this.state = state;
    }

}
