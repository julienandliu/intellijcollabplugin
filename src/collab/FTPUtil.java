package collab;

/**
 * Created by Lisa on 8/09/2016.
 */
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.intellij.openapi.application.ApplicationManager;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPSClient;
import org.bson.Document;

public class FTPUtil {

    // Mongo connection
    //private final String MONGO_HOST = "localhost";
    private final static String MONGO_HOST = "52.203.246.100";
    private final static int MONGO_PORT = 27017;

    /**
     * Download a whole directory from a FTP server.
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
     * @param parentDir Path of the parent directory of the current directory being
     * downloaded.
     * @param currentDir Path of the current directory being downloaded.
     * @param saveDir path of directory where the whole remote directory will be
     * downloaded and saved.
     * @throws IOException if any network or IO error occurred.
     */
    public static void downloadDirectory(FTPSClient ftpClient, String parentDir,
                                         String currentDir, String saveDir, boolean isRoot, String projectName) throws IOException {


        System.out.println("IN DOWNLOAD DIRECTORY");

        String dirToList = parentDir;
        if (!currentDir.equals("")) {
            dirToList += "/" + currentDir;
        }

        File file = new File(saveDir);

        if (!file.exists()) {
            System.out.println("No Folder");
            file.mkdir();
            System.out.println("Folder created");
        }

        FTPFile[] subFiles = ftpClient.listFiles(dirToList);

        if (subFiles != null && subFiles.length > 0) {
            for (FTPFile aFile : subFiles) {
                String currentFileName = aFile.getName();

                String filePath = parentDir + "/" + currentDir + "/"
                        + currentFileName;
                if (currentDir.equals("")) {
                    filePath = parentDir + "/" + currentFileName;
                }


                String newDirPath;

                if(!isRoot) {

                    newDirPath = saveDir + currentDir + File.separator + currentFileName;
                } else {

                    newDirPath = saveDir + File.separator +
                            projectName + File.separator + currentFileName;
                    //newDirPath = saveDir + File.separator + currentFileName;
                }


                if (currentDir.equals("")) {
                    newDirPath = saveDir + parentDir + File.separator
                            + currentFileName;
                }

                if (aFile.isDirectory()) {
                    // create the directory in saveDir
                    File newDir = new File(newDirPath);
                    boolean created = newDir.mkdirs();
                    if (created) {
                        System.out.println("CREATED the directory: " + newDirPath);
                    } else {
                        System.out.println("COULD NOT create the directory: " + newDirPath);
                    }


                    String realSaveDir;

                    if (saveDir.charAt(saveDir.length()-1) != '/') {
                        realSaveDir = saveDir + File.separator + projectName + File.separator;
                    } else {
                        realSaveDir = saveDir + projectName + File.separator;
                    }



                    // download the sub directory
                    downloadDirectory(ftpClient, dirToList, currentFileName,
                            realSaveDir, false, projectName);
                } else {
                    // download the file
                    boolean success = downloadSingleFile(ftpClient, filePath,
                            newDirPath, true);
                    if (success) {
                        System.out.println("DOWNLOADED the file: " + filePath);
                    } else {
                        System.out.println("COULD NOT download the file: "
                                + filePath);
                    }
                }
            }
        }
    }

    /**
     * Download a single file from the FTP server
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
     * @param remoteFilePath path of the file on the server
     * @param savePath path of directory where the file will be stored
     * @return true if the file was downloaded successfully, false otherwise
     * @throws IOException if any network or IO error occurred.
     */
    public static boolean downloadSingleFile(FTPSClient ftpClient,
                                             String remoteFilePath, String savePath,
                                             boolean replace) throws IOException {

        File downloadFile = new File(savePath);

        if (!replace && downloadFile.exists()) {
            System.out.println("File exists at location on disk, so not overwriting");
            return true;
        }

        // Get the file id from the database
        Activator activator = ApplicationManager.getApplication().getComponent(Activator.class);
        IntellijCollabSingleton main = activator.getSingleton();
        int projectId = main.getCurrentlyConnectedProject().getProjectID();
        String projectName = main.getCurrentlyConnectedProject().getProjectName();

        String saveDirPath = com.intellij.ide.impl.ProjectUtil.getBaseDir();
        String projectPath = saveDirPath + "/" + projectName;

        String filePath = savePath.replace(projectPath, "");

        if (!filePath.contains("/bin/")) { // Don't fetch id's of files in the bin folder
            int fileId = getFileID(projectId, filePath);
            try {
                main.putFilePathToIDMapping(filePath, fileId);
            } catch (Exception e) {

            }
        }

        File parentDir = downloadFile.getParentFile();
        if (!parentDir.exists()) {
            parentDir.mkdir();
        }

        OutputStream outputStream = new BufferedOutputStream(
                new FileOutputStream(downloadFile));
        try {
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            return ftpClient.retrieveFile(remoteFilePath, outputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    public static void listDirectory(FTPSClient ftpClient, String parentDir,
                                     String currentDir, int level) throws IOException {
        String dirToList = parentDir;
        if (!currentDir.equals("")) {
            dirToList += "/" + currentDir;
        }
        FTPFile[] subFiles = ftpClient.listFiles(dirToList);
        if (subFiles != null && subFiles.length > 0) {
            for (FTPFile aFile : subFiles) {
                String currentFileName = aFile.getName();
                for (int i = 0; i < level; i++) {
                    System.out.print("\t");
                }
                if (aFile.isDirectory()) {
                    System.out.println("[" + currentFileName + "]");
                    listDirectory(ftpClient, dirToList, currentFileName, level + 1);
                } else {
                    System.out.println(currentFileName);
                }
            }
        }
    }

    /**
     * Download a whole directory from a FTP server - not replacing files that
     * already exist
     * @param ftpClient an instance of org.apache.commons.net.ftp.FTPClient class.
     * @param parentDir Path of the parent directory of the current directory being
     * downloaded.
     * @param currentDir Path of the current directory being downloaded.
     * @param saveDir path of directory where the whole remote directory will be
     * downloaded and saved.
     * @throws IOException if any network or IO error occurred.
     */
    public static void updateDirectory(FTPSClient ftpClient, String parentDir,
                                       String currentDir, String saveDir, boolean isRoot) throws IOException {
        String dirToList = parentDir;
        if (!currentDir.equals("")) {
            dirToList += "/" + currentDir;
        }

        File file = new File(saveDir);

        if (!file.exists()) {
            System.out.println("No Folder");
            file.mkdir();
            System.out.println("Folder created");
        }

        FTPFile[] subFiles = ftpClient.listFiles(dirToList);

        if (subFiles != null && subFiles.length > 0) {
            for (FTPFile aFile : subFiles) {
                String currentFileName = aFile.getName();

                String filePath = parentDir + "/" + currentDir + "/"
                        + currentFileName;
                if (currentDir.equals("")) {
                    filePath = parentDir + "/" + currentFileName;
                }


                String newDirPath;

                if(!isRoot) {

                    newDirPath = saveDir + currentDir + File.separator + currentFileName;
                } else {

                    newDirPath = saveDir + File.separator + currentFileName;
                }


                if (currentDir.equals("")) {
                    newDirPath = saveDir + parentDir + File.separator
                            + currentFileName;
                }

                if (aFile.isDirectory()) {
                    // create the directory in saveDir
                    File newDir = new File(newDirPath);
                    boolean created = newDir.mkdirs();
                    if (created) {
                        System.out.println("CREATED the directory: " + newDirPath);
                    } else {
                        System.out.println("COULD NOT create the directory: " + newDirPath);
                    }


                    String realSaveDir;

                    if (saveDir.charAt(saveDir.length()-1) != '/') {
                        realSaveDir = saveDir + File.separator;
                    } else {
                        realSaveDir = saveDir + File.separator;
                    }



                    // download the sub directory
                    updateDirectory(ftpClient, dirToList, currentFileName,
                            realSaveDir, false);
                } else {
                    // download the file
                    boolean success = downloadSingleFile(ftpClient, filePath,
                            newDirPath, false);
                    if (success) {
                        System.out.println("DOWNLOADED the file: " + filePath);
                    } else {
                        System.out.println("COULD NOT download the file: "
                                + filePath);
                    }
                }
            }
        }
    }

    private static int getFileID(int projectID, String filePath) {
        MongoClient mongoClient = new MongoClient(MONGO_HOST, MONGO_PORT);
        MongoDatabase db = mongoClient.getDatabase("testdb");

     //   System.out.println("Getting file ID from mongo for filePath: " + filePath + ", in project: " + projectID);
        int fileID = 1;

        List<BasicDBObject> and = new ArrayList<BasicDBObject>();
        BasicDBObject andQuery = new BasicDBObject();
        and.add(new BasicDBObject("projectId", projectID));
        and.add(new BasicDBObject("path", filePath));
        andQuery.put("$and", and);

        FindIterable<org.bson.Document> iterable =  db.getCollection("projectElements").find(andQuery);

        if (!iterable.iterator().hasNext()) {
            System.out.println("File ID does not exist");
            mongoClient.close();
        }

        for (Document current : iterable) {
            fileID = current.getInteger("_id");
        }

        mongoClient.close();
        return fileID;


    }
}
