package collab;

import com.intellij.openapi.components.ApplicationComponent;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Lisa on 8/09/2016.
 */
public class Activator implements ApplicationComponent {

    // To manage the state of the plugin
    private IntellijCollabSingleton singleton;
    // The keep track of the state of the IDE
    private IDEManager ide;


    @Override
    public void initComponent() {
        this.singleton = new IntellijCollabSingleton();
        this.ide = new IDEManager();
    }

    @Override
    public void disposeComponent() {
        //TODO: Close connection with node for existing project
        singleton.getMongoClient().close();
        System.out.println("Bye");
    }

    public IDEManager getIDEManager() {
        return this.ide;
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "Intellij Collaboration Plugin";
    }

    public IntellijCollabSingleton getSingleton() {
        return singleton;
    }
}
