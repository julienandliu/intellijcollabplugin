package collab;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.vfs.*;
import org.jetbrains.annotations.NotNull;


/**
 * Created by Lisa on 11/09/2016. This class listens to files that gets change
 * in the IDE. e.g. Creating/modifying/deleting classes, files, folders, etc.
 * This is different to IDEListener that listens to changes from the
 * code editor that is open.
 *
 * 16 October: Will leave this incomplete, as not required for user testing.
 * May come back to it, and fix it so it runs for demo day in week 12.
 */
public class FileListener implements VirtualFileListener {


    public FileListener() {
    }

    @Override
    public void propertyChanged(@NotNull VirtualFilePropertyEvent virtualFilePropertyEvent) {

    }

    @Override
    public void contentsChanged(@NotNull VirtualFileEvent virtualFileEvent) {

    }

    /**
     * Gets called when a file or folder is created in the IDE.
     * @param virtualFileEvent
     */
    @Override
    public void fileCreated(@NotNull VirtualFileEvent virtualFileEvent) {

//        Activator activator = ApplicationManager.getApplication().getComponent(Activator.class);
//        IntellijCollabSingleton main = activator.getSingleton();
//        String messageToSend = null;
//
//        if (main.getCurrentlyConnectedProject() == null) {
//            return;
//        }
//
//        int projectId = main.getCurrentlyConnectedProject().getProjectID();
//        int userId = main.getUserID();
//        String requestID = main.generateRandomRequestID();
//
//
//        VirtualFile virtualFile = virtualFileEvent.getFile();
//        String fileName = virtualFile.getName();
//        String packageName = getPackageName(virtualFile.getCanonicalPath());
//        boolean isDirectory = virtualFile.isDirectory();
//        String extension = virtualFile.getExtension();
//
//        if(isDirectory) {
//
//            if (!packageName.isEmpty()) {
//                packageName = packageName.concat(".");
//                packageName = packageName.concat(fileName);
//            } else {
//                packageName = packageName.concat(fileName);
//            }
//
//            messageToSend = getCreatePackageJsonMessage(packageName, projectId, userId, requestID);
//
//            System.out.println("We should create the following package: " + packageName.toString());
//
//            // send a message to create the package to the websocket
//        } else if (extension.equals("java")) {
//
//            System.out.println("we are creating a java class: " + fileName +
//            " in package: " + packageName);
//
//            messageToSend = getCreateClassJsonMessage(packageName, fileName, projectId, userId, requestID);
//
//        } else {
//            // Do nothing? I don't think is implemented in the web based version
//            System.out.println("We are creating a regular file");
//        }
//
//        // send to websocket
//        main.getProjectManagementWS().sendMessage(messageToSend);

    }

    /**
     * Not implemented in node or website
     * @param virtualFileEvent
     */
    @Override
    public void fileDeleted(@NotNull VirtualFileEvent virtualFileEvent) {

//        VirtualFile virtualFile = virtualFileEvent.getFile();
//
//        String fileName = virtualFile.getName();
//        String packageName = getPackageName(virtualFile.getCanonicalPath());
//        boolean isDirectory = virtualFile.isDirectory();
//        String extension = virtualFile.getExtension();
//
//        if(isDirectory) {
//
//            if (!packageName.isEmpty()) {
//                packageName = packageName.concat(".");
//                packageName = packageName.concat(fileName);
//            } else {
//                packageName = packageName.concat(fileName);
//            }
//            System.out.println("We should delete the following package: " + packageName.toString());
//
//            // send a message to create the package to the websocket
//        } else if (extension.equals("java")) {
//            System.out.println("we are deleting a java class" + fileName +
//                    " in package: " + packageName);
//
//        } else {
//            // Do nothing? I don't think is implemented in the web based version
//            System.out.println("We are deleting a regular file");
//        }

    }

//    private String getPackageName(String path) {
//        String [] pathItems = path.split("/");
//
//        boolean processed = false;
//        StringBuilder filePath =  new StringBuilder();
//
//        for (int i = 0; i < pathItems.length - 1; i++) {
//            if (!processed) {
//                if (pathItems[i].equals("src")) {
//                    processed = true;
//                }
//                continue;
//            }
//                filePath.append(pathItems[i]);
//                filePath.append('.');
//
//        }
//
//        if (filePath.length() > 1) {
//            filePath.deleteCharAt(filePath.length()-1);
//        }
//
//        return filePath.toString();
//    }

    @Override
    public void fileMoved(@NotNull VirtualFileMoveEvent virtualFileMoveEvent) {

    }

    @Override
    public void fileCopied(@NotNull VirtualFileCopyEvent virtualFileCopyEvent) {

    }

    @Override
    public void beforePropertyChange(@NotNull VirtualFilePropertyEvent virtualFilePropertyEvent) {

    }

    @Override
    public void beforeContentsChange(@NotNull VirtualFileEvent virtualFileEvent) {

    }

    @Override
    public void beforeFileDeletion(@NotNull VirtualFileEvent virtualFileEvent) {

    }

    @Override
    public void beforeFileMovement(@NotNull VirtualFileMoveEvent virtualFileMoveEvent) {

    }

    /*************************************************
     * Private helper methods
     *************************************************/


    private String getCreateClassJsonMessage(String packageName, String className,
                                 int projectId, int userId, String requestID) {

        JsonObject obj = new JsonObject();
        obj.add("action", "createClass");
        obj.add("requestID", requestID);
        obj.add("userId", userId);
        obj.add("projectId", projectId);
        obj.add("packageName", packageName);
        obj.add("name", className);

        return obj.toString();


    }


    private String getCreatePackageJsonMessage(String packageName, int projectId,
                                         int userId, String requestID) {

        JsonObject obj = new JsonObject();
        obj.add("action", "createPackage");
        obj.add("requestID", requestID);
        obj.add(" userId", userId);
        obj.add("projectId", projectId);
        obj.add("packageName", "");
        obj.add("name", packageName);

        return obj.toString();


    }

}
