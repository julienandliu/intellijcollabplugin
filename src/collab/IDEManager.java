package collab;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.psi.PsiDocumentManager;
import ot.ClientManager;
import ot.TextOperation;

import java.util.HashMap;

/**
 * Created by Lisa on 15/09/2016.
 */
public class IDEManager {

    // Map file id -> file content
    private HashMap<Integer, String> openedProjectFiles;
    // Keep track of the client manager for each file
    private HashMap<Integer, ClientManager> fileClients;

    // Keep track of the document currently broughtToTop and its listener
    private Document currentDocument = null;
    private IDEDocumentListener activeDocumentListener = new IDEDocumentListener();

    public IDEManager() {
        openedProjectFiles = new HashMap<>();
        fileClients = new HashMap<>();
    }

    public void insertChangesInEditor(TextOperation operation) {

        Activator activator = ApplicationManager.getApplication().getComponent(Activator.class);

        int fileId = activator.getSingleton().getCurrentlyOpenFileId();
        String document = getContentForFileId(fileId);

        int offset = 0;

        for (Object op : operation.getOps()) {

            if (operation.isRetain(op)) {
                System.out.println("retain: " + op.toString());
                offset += (int) op;

            } else if (operation.isInsert(op)) {
                System.out.println("insert: " + op.toString());

                updateEditor(offset, 0, (String) op);

                offset += ((String) op).length();

            } else if (operation.isDelete(op)) {
                System.out.println("delete: " + op.toString());

                updateEditor(offset, -((int) op), "");

                offset += (int) op; // delete is < 0

            } else {
                // Shouldn't get here
                System.out.println("SHOULD NEVER GET THERE - Unknown op type, op: " + op.toString());
            }
        }

    }

    private void updateEditor(int offset, int length, String text) {

        // There should only be one project open at a time by our assumption
        Project[] projects = ProjectManager.getInstance().getOpenProjects();



        currentDocument.removeDocumentListener(activeDocumentListener);
        int endOffset = offset + length;


        //New instance of Runnable to make a replacement
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                currentDocument.replaceString(offset, endOffset, text);
                PsiDocumentManager.getInstance(projects[0]).commitDocument(currentDocument);
            }
        };

        //Making the replacement
        WriteCommandAction.runWriteCommandAction(projects[0], runnable);


        currentDocument.addDocumentListener(activeDocumentListener);
    }

    public void storeContentForFileId(int fileId, String fileContent) {
        openedProjectFiles.put(fileId, fileContent);
    }

    public String getContentForFileId(int fileId) {
        return openedProjectFiles.get(fileId);
    }

    public void storeClientForFileId(int fileId, ClientManager client) {
        fileClients.put(fileId, client);
    }

    public ClientManager getClientForFileId(int fileId) {
        return fileClients.get(fileId);
    }

    public void setCurrentDocument(Document doc) {
        if(currentDocument != null) {
            currentDocument.removeDocumentListener(activeDocumentListener);
        }
        currentDocument = doc;
        currentDocument.addDocumentListener(activeDocumentListener);
    }

    public Document getCurrentDocument() {
        return currentDocument;
    }

}
