package collab;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import messaging.FileContentResponseMessage;
import misc.Project;
import org.bson.Document;
import ui.LoginDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * This class is the central entity for coordinating activities in the IDE.
 * Stores the state, performs initialisation and various other things.
 */
public class IntellijCollabSingleton {

    // User related
    private String username;
    private String password;
    private int userId;
    private boolean loggedIn;

    // The web sockets
    private ProjectFactoryWS projectFactoryWS;
    private ProjectManagementWS projectManagementWS;
    private RealTimeWS realTimeWS;

    //Websocket address
    private final String PROJECT_FACTORY_ADDRESS = "ws://52.203.246.100:8083";
    private final String PROJECT_MANAGEMENT_ADDRESS = "ws://52.203.246.100:8084";
    private final String REAL_TIME_ADDRESS = "ws://52.203.246.100:8085";

    // Mongo connection
    private final String MONGO_HOST = "52.203.246.100";
    private final int MONGO_PORT = 27017;
    MongoClient mongoClient;

    // List of projects for monitoring (to collaborate) when online
    private Project currentlyConnectedProject;
    private int currentlyOpenFileId;

    // Message response queues
    private HashMap<String, FileContentResponseMessage> projectManagementInboundQueue;

    // Persistent storage between Intellij sessions
    private PropertiesComponent properties;


    public IntellijCollabSingleton() {

        // initialise message queues
        this.projectManagementInboundQueue = new HashMap<>();

        // Initalise websockets
        this.projectFactoryWS = new ProjectFactoryWS(PROJECT_FACTORY_ADDRESS);
        this.projectManagementWS = new ProjectManagementWS(
                PROJECT_MANAGEMENT_ADDRESS, projectManagementInboundQueue);
        this.realTimeWS = new RealTimeWS(REAL_TIME_ADDRESS);


        this.currentlyConnectedProject = null;
        this.currentlyOpenFileId = -1;
        this.properties = PropertiesComponent.getInstance();
        this.mongoClient = new MongoClient(MONGO_HOST, MONGO_PORT);

        this.loggedIn = login();
        new Thread(() -> connect()).start();

        VirtualFileManager.getInstance().addVirtualFileListener(new FileListener());
    }

    public boolean connect() {

        try {
            return projectFactoryWS.connect() &&
                    projectManagementWS.connect() && realTimeWS.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    /*************************************************
     * Project related methods
     *************************************************/

    /**
     * Send a message to Synchronisation controller web socket
     * @param project
     */
    private void deregisterProjectFromCollaboration(Project project) {
        JsonObject obj = new JsonObject();
        obj.add("action", "closeConnection");
        obj.add("userId", userId);
        obj.add("projectId", project.getProjectID());

        JsonArray list = new JsonArray();
        list.add(currentlyOpenFileId);

        obj.add("openedFilesId", list);

        // send to websocket
        realTimeWS.sendMessage(obj.toString());
    }

    /**
     * Send a message to synchronisation controller web socket
     * @param project
     */
    private void registerProjectForCollaboration(Project project) {
        JsonObject obj = new JsonObject();
        obj.add("action", "initConnection");
        obj.add("requestID", generateRandomRequestID());
        obj.add("userId", userId);
        obj.add("projectId", project.getProjectID());

        // send to websocket
        realTimeWS.sendMessage(obj.toString());

    }

    public void setConnectedProject(String projectName, String projectPath) {

        if (projectName == null) {
            currentlyConnectedProject = null;
            return;
        }

        // need to check whether this has changed. if it has need to update node
        Project newProject = new Project(projectName, getProjectID(projectName), projectPath);

        if (currentlyConnectedProject == null) {
            currentlyConnectedProject = newProject;
            registerProjectForCollaboration(newProject);
            return;
        }

        if (newProject.equals(currentlyConnectedProject)) {
            //do nothing.
        } else {
            deregisterProjectFromCollaboration(currentlyConnectedProject);
            currentlyConnectedProject = newProject;
            registerProjectForCollaboration(newProject);
        }

    }


    public void putProjectNameToIDMapping(String projectName, int projectID) {
        //return projectIDMapping.put(projectName, projectID)
        properties.setValue(projectName, projectID, -1);
    }

    /**
     * If the project is to be disconnected from the system, it's default
     * project id is -1.
     * @param projectName
     */
    public void removeProjectNameToIDMapping(String projectName) {
        properties.setValue(projectName, -1, -1);
    }

    public boolean isProjectConnected(String projectName) {
        Project toCheck = new Project(projectName, getProjectID(projectName));
        return currentlyConnectedProject.equals(toCheck);
    }


    public boolean isDisconnected() {
        return projectFactoryWS.disconnect() &&
                projectManagementWS.disconnect() && realTimeWS.disconnect();
    }

    /*************************************************
     * Public Utility Methods
     *************************************************/

    /**
     * Returns -1 if project is not associated with a project id.
     * Assuming that the project id is an integer for now
     * @param projectName
     * @return
     */
    public int getProjectID(String projectName) {
        //return projectIDMapping.get(projectName, null);
        return properties.getInt(projectName, -1);
    }

    public String generateRandomRequestID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }


    public int getFileID(String filePath) {
        return properties.getInt(filePath, -1);
    }

    public void putFilePathToIDMapping(String filePath, int fileID) {
        properties.setValue(filePath, fileID, -1);
    }

    public void removeFilePathToIDMapping(String filePath) {
        properties.setValue(filePath, -1, -1);
    }


    /**
     * Connect to the mongo database, and query for the user by it's username
     * and password.
     */
    public boolean validateCredentials() {
        MongoDatabase db = mongoClient.getDatabase("testdb");

        BasicDBObject andQuery = new BasicDBObject();
        List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
        obj.add(new BasicDBObject("email", username));
        obj.add(new BasicDBObject("password", password));
        andQuery.put("$and", obj);

        FindIterable<Document> iterable =  db.getCollection("users").find(andQuery);

        if (!iterable.iterator().hasNext()) {
            System.out.println("User doesn't exist");
            return false;
        } else {
            System.out.println("User exists");
        }

        // Get the user's id
        FindIterable<org.bson.Document> userIDIterable =
                db.getCollection("users").find(new Document("email", username));

        for (Document current : userIDIterable) {
            userId = current.getInteger("_id");

        }

        return true;
    }


    /*************************************************
     * Private helper methods
     *************************************************/


    /**
     * Check if the username is stored in persistent storage. If so, retrieve
     * it, and try and validate the credentials.
     * @return
     */
    private boolean login() {

//        String username = properties.getValue("emailAddress", "");
//        String password = properties.getValue("password", "");
//
//        System.out.println("Username: " + username);
//
//        if ((username == "") || (password == "")) {
//            return false;
//        }

        String[] credentials = LoginDialog.main();

        String username = credentials[0];
        String password = credentials[1];

        setUserCredentials(username, password);

        return validateCredentials();
      //  return false;
    }

    public boolean checkConnected() {
        return projectFactoryWS.isConnected() &&
                projectManagementWS.isConnected() && realTimeWS.isConnected();
    }

    public void disconnect() {
        this.username = null;
        this.password = null;
        properties.setValue("emailAddress", null);
        properties.setValue("password", null);
        loggedIn = false;
        deregisterProjectFromCollaboration(currentlyConnectedProject);
        currentlyConnectedProject = null;
    }



    /*************************************************
     * Getter methods
     *************************************************/

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String[] getCredentials() {
        return new String[] {username, password};
    }

    public boolean checkCredentialsStored() {
        return (username != null) && (password != null);
    }

    public ProjectFactoryWS getProjectFactoryWS() {
        return projectFactoryWS;
    }

    public ProjectManagementWS getProjectManagementWS() {
        return projectManagementWS;
    }

    public RealTimeWS getRealTimeWS() {
        return realTimeWS;
    }

    public int getUserID() {
        return userId;
    }

    public Project getCurrentlyConnectedProject() {
        return currentlyConnectedProject;
    }

    public int getCurrentlyOpenFileId() {
        return currentlyOpenFileId;
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }


    /*************************************************
     * Setter methods
     *************************************************/

    public void setUserCredentials(String username, String password) {
        this.username = username;
        this.password = password;

//        properties.setValue("emailAddress", username);
//        properties.setValue("password", password);
    }

    public void setCurrentlyOpenFileId(int fileId) {
        this.currentlyOpenFileId = fileId;
    }


}
