package collab;

/**
 * Created by Lisa on 8/09/2016. This particular websocket is mostly used to send
 * and receive file content from node.
 */
import java.util.HashMap;

import com.google.gson.Gson;

import messaging.FileContentResponseMessage;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;


@WebSocket(maxTextMessageSize = 64 * 1024)
public class ProjectManagementWS extends CommunicationWebSocket {

    private HashMap<String, FileContentResponseMessage> inboundMessages;
    private Gson gson = new Gson();

    public ProjectManagementWS(String destination, HashMap<String, FileContentResponseMessage> inboundMessages) {
        this.destination = destination;
        this.inboundMessages = inboundMessages;

    }

    /**
     * Callback hook for Message Events. This method will be invoked when a
     * client send a message.
     */
    //@Override
    @OnWebSocketMessage
    public void onMessage(String message) {

        System.out.println("In onMessage - ProjectManagementWS");

        System.out.println("message: " + message);

        // Process the message here
        FileContentResponseMessage response = gson.fromJson(message, FileContentResponseMessage.class);

        String requestID = response.getRequestID();

        inboundMessages.put(requestID, response);

    }

    public FileContentResponseMessage getResponseMessage(String requestID) {

        while(!inboundMessages.containsKey(requestID)) {
            // do nothing - response not yet arrived.
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        FileContentResponseMessage response = inboundMessages.get(requestID);
        inboundMessages.remove(requestID);

        return response;
    }
}
