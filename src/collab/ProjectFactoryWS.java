package collab;


import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

/**
 * Created by Lisa on 8/09/2016.
 */
@WebSocket(maxTextMessageSize = 64 * 1024)
public class ProjectFactoryWS extends CommunicationWebSocket {

    public ProjectFactoryWS(String destination) {
        this.destination = destination;
    }


    /**
     * Callback hook for Message Events. This method will be invoked when a client send a message.
     */
   // @Override
    @OnWebSocketMessage
    public void onMessage(String message) {
        // Process the message here
        System.out.println("In onMessage - ProjectManagementWS");

    }



}
