package collab;

import com.intellij.openapi.application.ApplicationManager;;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import ot.TextOperation;


/**
 * Created by Lisa on 11/09/2016.
 */
public class IDEDocumentListener implements DocumentListener {


    /**
     * This class listens to changes made by the user on IDE, applies OT,
     * and sends update to Node
     */
    public IDEDocumentListener() {
    }


    @Override
    public void beforeDocumentChange(DocumentEvent documentEvent) {

    }


    /**
     * Basically this method gets called when someone types into the editor.
     * The fragments are char sequences. If someone is typing something, then
     * the newFragment thing will have something. Otherwise, the oldFragment
     * thing will have what the user deleted.
     * @param documentEvent
     */
    @Override
    public void documentChanged(DocumentEvent documentEvent) {

        /**     Uncomment for debugging
         * System.out.println("Document changed!");
         System.out.println("New fragment: " + documentEvent.getNewFragment());
         System.out.println("At position: " + documentEvent.getOffset());
         System.out.println("Old fragment: " + documentEvent.getOldFragment());
         System.out.println("Old fragment length " + documentEvent.getOldFragment().length());
         System.out.println("New fragment length " + documentEvent.getNewFragment().length());
         */

        Activator activator = ApplicationManager.getApplication().getComponent(Activator.class);
        IDEManager ide = activator.getIDEManager();
        IntellijCollabSingleton main = activator.getSingleton();
        int fileId = main.getCurrentlyOpenFileId();
        String document = ide.getContentForFileId(fileId);

        int characterCountBefore = documentEvent.getOffset();
        int characterCountAfter = document.length() - documentEvent.getOffset();

        TextOperation operation = new TextOperation();


        // A replace works by first doing a delete, then an insert as 2 separate events
        if (documentEvent.getOldFragment().length() > 0) {  //delete
            int length = documentEvent.getOldFragment().length();

            operation.retain(characterCountBefore)
                    .delete(length).
                    retain(characterCountAfter - length);

        } else if (documentEvent.getNewFragment().length() > 0) {   //insert
            int length = documentEvent.getNewFragment().length();
            operation.retain(characterCountBefore)
                    .insert(documentEvent.getNewFragment().toString())
                    .retain(characterCountAfter);

        } else {
            System.out.println("SHOULD NEVER GET HERE - Unknown operation");
            return;
        }

        // Update the document
        ide.storeContentForFileId(fileId, operation.apply(document));

        // Will trigger client.sendOperation
        ide.getClientForFileId(fileId).applyClient(operation);

    }
}
