package collab;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.util.messages.MessageBus;
import org.apache.commons.net.ftp.FTPSClient;
import org.jetbrains.annotations.NotNull;

/**
 * Created by Lisa on 10/09/2016.
 */
public class ProjectService implements ProjectComponent {

    private Project theProject;
    private Activator activator;
    private IntellijCollabSingleton main;
    String server = "52.203.246.100";
    String user = "hello";
    String pass = "world";
    FTPSClient ftpClient = new FTPSClient(true);
    private FileListener fileListener = new FileListener();

    public ProjectService(Project project) {
        this.theProject = project;
    }


    @Override
    public void projectOpened() {
        // Check if the project is listed for collaboration
        if ((main.getProjectID(theProject.getName()) != -1) && main.isLoggedIn()) {
            registerProjectForCollaboration();
            main.setConnectedProject(theProject.getName(), theProject.getBasePath());
        }
    }

    @Override
    public void projectClosed() {
        System.out.println("Project closed: " + theProject.getName());
        if (main.getProjectID(theProject.getName()) != -1) {
            deregisterProjectFromCollaboration();
            main.setConnectedProject(null, null);
        }
    }

    @Override
    public void initComponent() {
        if (main == null) {
            activator = ApplicationManager.getApplication().getComponent(Activator.class);
            main = activator.getSingleton();
        }

        String projectName = theProject.getName();
        int projectID = main.getProjectID(projectName);

        // Check if the project is a collaborative one
        if (projectID == -1 ) {
            return;
        }

        // Check if we have login credentials available, validate them
        if (main.isLoggedIn()) {
            // If so, update the project
            // Download the project
            try {
                // connect and login to the server
                ftpClient.connect(server);
                ftpClient.login(user, pass);

                // Set protection buffer size
                ftpClient.execPBSZ(0);
                // Set data channel protection to private
                ftpClient.execPROT("P");

                // use local passive mode to pass firewall
                ftpClient.enterLocalPassiveMode();

                // Default Intellij project location
                String saveDirPath = com.intellij.ide.impl.ProjectUtil.getBaseDir();
                String remoteDirPath = "workspace";
                String downloadedPath = saveDirPath + "/" + projectName;
                String stringProjectID = Integer.toString(projectID);

           //     FTPUtil.updateDirectory(ftpClient, remoteDirPath, stringProjectID, downloadedPath, true);

                Project[] projects = ProjectManager.getInstance().getOpenProjects();

                for (Project project : projects) {
                    project.getBaseDir().refresh(false,true);
                }

                // log out and disconnect from the server
                ftpClient.logout();
                ftpClient.disconnect();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @Override
    public void disposeComponent() {
        theProject = null;
        activator = null;
        main = null;
        //theProject.dispose();
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "Project Manager";
    }

    /*************************************************
     * Private helper methods
     *************************************************/

    private void registerProjectForCollaboration() {

        IDEListener ide = new IDEListener();

        String projectName = theProject.getName();

        // check that this is ok
        String projectPath = theProject.getProjectFilePath();

       // main.setConnectedProject(projectName, projectPath);
        MessageBus messageBus = theProject.getMessageBus();
        messageBus.connect().subscribe(
                FileEditorManagerListener.FILE_EDITOR_MANAGER, ide);
        messageBus.connect().subscribe(
                FileEditorManagerListener.Before.FILE_EDITOR_MANAGER, ide);
    }

    /**
     * TODO: Need to check whether this gets called when the user just closes
     * the application directly.
     */
    private void deregisterProjectFromCollaboration() {

        Activator activator = ApplicationManager.getApplication().getComponent(Activator.class);
        IntellijCollabSingleton main = activator.getSingleton();
        int userId = main.getUserID();
        int currentlyOpenFileId = main.getCurrentlyOpenFileId();
        int projectId = main.getProjectID(theProject.getName());

        misc.Project project = main.getCurrentlyConnectedProject();

        if (projectId != -1 && (new misc.Project(theProject.getName(), projectId).equals(project))) {

            JsonObject obj = new JsonObject();
            obj.add("action", "closeConnection");
            obj.add("userId", userId);
            obj.add("projectId", project.getProjectID());

            JsonArray list = new JsonArray();
            list.add(currentlyOpenFileId);

            obj.add("openedFilesId", list);

            // send to websocket
            if (main.getRealTimeWS().isConnected()) {
                main.getRealTimeWS().sendMessage(obj.toString());
            }
        }

      //  VirtualFileManager.getInstance().removeVirtualFileListener(fileListener);

    }

    public Project getTheProject() {
        return theProject;
    }

}


