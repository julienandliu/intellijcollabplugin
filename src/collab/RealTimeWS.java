package collab;

/**
 * Created by Lisa on 8/09/2016.
 */


import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.project.ex.ProjectManagerEx;
import com.intellij.openapi.vfs.VirtualFileManager;
import org.apache.commons.net.ftp.FTPSClient;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import ot.TextOperation;

@WebSocket(maxTextMessageSize = 64 * 1024)
public class RealTimeWS extends CommunicationWebSocket {

    String server = "52.203.246.100";
    String user = "hello";
    String pass = "world";
    FTPSClient ftpClient = new FTPSClient(true);
    private Activator activator;
    private IntellijCollabSingleton main;

    public RealTimeWS(String destination) {
        this.destination = destination;
    }

    /**
     * Callback hook for Message Events. This method will be invoked when a client send a message.
     */
   // @Override
    @OnWebSocketMessage
    public void onMessage(String message) {
        // Process the message here
        JsonObject object = Json.parse(message).asObject();
        String action = object.get("action").asString();

        /**
         * Possible actions:
         * initConnection, operation, notification, compilation
         */
        switch(action) {
            case "notification":
                handleNotification(object);
                break;
            case "operation":
                handleOperation(object);
                break;
            default:
                break;

        }
    }

    /**
     * JSON objects are of the form:
     * {
     * 	"action": "notification",
     * 	"type": "projectStructureUpdate",
     * [ "byUser": user name,
     *	"collaboratorsConnected": [collaborators],
     *	"userID": userID,
     *	"fileID": fileID  ]  --> optional fields depending on notification type
     * @param object
     */
    private void handleNotification(JsonObject object) {

        String type = object.get("type").asString();

        switch (type) {
            case "projectStructureUpdate":
                new Thread(() -> synchroniseProjectStructure()).start();
                break;
            case "collaboratorsConnected":
                //TODO?
                break;
            case "activeFileSet":
                //TODO?
                break;
            default:
                break;
        }

    }

    private void handleOperation(JsonObject msgJson) {

        activator = ApplicationManager.getApplication().getComponent(Activator.class);
        main = activator.getSingleton();

        TextOperation operation = new TextOperation();
        operation.fromJson(msgJson.get("operation").asArray());
        int fileId = msgJson.get("fileId").asInt();
        int userId =  msgJson.get("userId").asInt();

        System.out.println("received operation: " + msgJson.get("operation").toString() + " from user: " + userId);

        IDEManager ide = activator.getIDEManager();

        if (userId == main.getUserID()) {
            ide.getClientForFileId(fileId).serverAck();
        } else {
            ide.getClientForFileId(fileId).applyServer(operation);
        }
    }

    /**
     * Updates insteads of downloading the entire folder set again.
     */
    private void synchroniseProjectStructure() {

        activator = ApplicationManager.getApplication().getComponent(Activator.class);
        main = activator.getSingleton();

        // Get the currently connected project
        misc.Project currentlyConnectedProject = main.getCurrentlyConnectedProject();
        String projectName = currentlyConnectedProject.getProjectName();

        // Download the project
        try {
            // connect and login to the server
            ftpClient.connect(server);
            ftpClient.login(user, pass);

            // Set protection buffer size
            ftpClient.execPBSZ(0);
            // Set data channel protection to private
            ftpClient.execPROT("P");

            // use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

            // Default Intellij project location
            String saveDirPath = com.intellij.ide.impl.ProjectUtil.getBaseDir();
            String remoteDirPath = "workspace";
            String downloadedPath = saveDirPath + "/" + projectName;
            String projectID = Integer.toString(currentlyConnectedProject.getProjectID());

            FTPUtil.updateDirectory(ftpClient, remoteDirPath, projectID, downloadedPath, true);

            Project[] projects = ProjectManager.getInstance().getOpenProjects();

            for (Project project : projects) {
                project.getBaseDir().refresh(false,true);
            }

            // log out and disconnect from the server
            ftpClient.logout();
            ftpClient.disconnect();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


}
