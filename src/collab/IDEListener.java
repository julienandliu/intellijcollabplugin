package collab;

import com.eclipsesource.json.JsonObject;
import com.google.gson.Gson;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerAdapter;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import messaging.ActiveFileSetUpdateRequestMessage;
import messaging.FileContentRequestMessage;
import messaging.FileContentResponseMessage;
import org.jetbrains.annotations.NotNull;
import ot.ClientManager;


/**
 * Created by Lisa on 10/09/2016. All the methods in this class are only triggered
 * if they were downloaded from the web for collaboration purposes (and
 * when we get around to implementing it, uploaded to the web).
 */
public class IDEListener extends FileEditorManagerAdapter implements FileEditorManagerAdapter.Before {

    private Gson gson = new Gson();
    Activator activator;
    IntellijCollabSingleton main;

    public IDEListener() {
        activator = ApplicationManager.getApplication().getComponent(Activator.class);
        main = activator.getSingleton();
    }

    /**
     * Do not use this method, it get's called after selectionChange, and so does
     * not behave like Eclipse. Using beforeFileOpened instead.
     * @param source
     * @param file
     */
    @Override
    public void fileOpened(@NotNull FileEditorManager source, @NotNull VirtualFile file) {
        super.fileOpened(source, file);
    }

    @Override
    public void fileClosed(@NotNull FileEditorManager source, @NotNull VirtualFile file) {

        Project theProject = null;
        Module module = null;


        Project[] projects = ProjectManager.getInstance().getOpenProjects();

        if (file != null && file.isInLocalFileSystem()) {
            for (Project project : projects) {
                module = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(file);
                if (module != null) {
                    theProject = project;
                }
            }
        }

        if ((module == null) || (theProject == null)) {
            return;
        }

        // The file belongs to this project so continue processing.
        String projectName = theProject.getName();
        int projectId = main.getProjectID(projectName);
        int userId = main.getUserID();
        int currentFileId = main.getCurrentlyOpenFileId();

        JsonObject obj = new JsonObject();
        obj.add("action", "fileClosed");
        obj.add("projectId", projectId);
        obj.add("userId", userId);
        obj.add("fileId", currentFileId);

        // send message to node
        main.getRealTimeWS().sendMessage(obj.toString());

        super.fileClosed(source, file);
        System.out.println("In file closed");
    }

    /**
     * Called whenever the editor changes contents (when a file is opened, this
     * is called, as well as fileOpened.
     * @param event
     */
    @Override
    public void selectionChanged(@NotNull FileEditorManagerEvent event) {

        super.selectionChanged(event);

        String projectPath;
        VirtualFile newFile = event.getNewFile();
        String fileLocation = null;
        Project theProject = null;
        Module module = null;

        Project[] projects = ProjectManager.getInstance().getOpenProjects();

        if (newFile != null && newFile.isInLocalFileSystem()) {
            for (Project project : projects) {
                module = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(newFile);
                if (module != null) {
                    theProject = project;
                }
            }
        }

        if ((module == null) || (theProject == null)) {
            return;
        }


        // The file belongs to this project so continue processing.
        fileLocation = newFile.getCanonicalPath();
        String projectName = theProject.getName();
        int projectId = main.getProjectID(projectName);
        final Project theFinalProject = theProject;
        projectPath = theFinalProject.getBasePath();
        fileLocation = fileLocation.replace(projectPath, "");

        // fetch the contents of the file from the web, and replace the editor with it
        if ((projectName != null) && (projectId != -1) && (fileLocation != null)) {

            main.setConnectedProject(projectName, projectPath);

            int fileId = main.getFileID(fileLocation);
            int userId = main.getUserID();

            // File doesn't exist
            if (fileId == -1) {
                return;
            }

            // Let the synchronisation controller know that this file is active
            String toSend = gson.toJson(new ActiveFileSetUpdateRequestMessage("activeFileSet", projectId, userId, fileId));
            main.getRealTimeWS().sendMessage(toSend);

            // Set the currently open fileId
            main.setCurrentlyOpenFileId(fileId);

            // Set the contents of the editor with the content
            Editor editor = FileEditorManager.getInstance(theProject).getSelectedTextEditor();
            final com.intellij.openapi.editor.Document document = editor.getDocument();

            IDEManager ide = activator.getIDEManager();
            String content = ide.getContentForFileId(fileId);

            //New instance of Runnable to make a replacement
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    document.setText(content);
                    PsiDocumentManager.getInstance(theFinalProject).commitDocument(document);
                }
            };

            //Making the replacement
            WriteCommandAction.runWriteCommandAction(theProject, runnable);

            ide.setCurrentDocument(document);
        }


    }

    @Override
    public void beforeFileOpened(@NotNull FileEditorManager fileEditorManager, @NotNull VirtualFile virtualFile) {
        System.out.println("In before file opened");

        String projectName = null;
        String fileLocation = virtualFile.getCanonicalPath();
        String projectPath = null;
        Module module = null;
        Project theProject = null;

        Project[] projects = ProjectManager.getInstance().getOpenProjects();

        if (virtualFile != null && virtualFile.isInLocalFileSystem()) {
            for (Project project : projects) {
                module = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(virtualFile);
                if (module != null) {
                    theProject = project;
                }
            }
        }

        if ((module == null) || (theProject == null)) {
            return;
        }

        final Project theFinalProject = theProject;
        projectPath = theFinalProject.getBasePath();
        projectName = theFinalProject.getName();
        fileLocation = fileLocation.replace(projectPath, "");

        // fetch the contents of the file from the web, and replace the editor with it
        if ((projectName != null) && (fileLocation != null)) {

            int userId = main.getUserID();
            int projectId = main.getProjectID(projectName);
            int fileId = main.getFileID(fileLocation);

            // Generate a random requestID
            String requestID = main.generateRandomRequestID();

            // Send request to the project management websocket
            String toSend = gson.toJson(new FileContentRequestMessage("getFileContent", requestID, userId, projectId, fileId));

            // Wait for the response
            main.getProjectManagementWS().sendMessage(toSend);

            // Extract the content,
            FileContentResponseMessage response = main.getProjectManagementWS().getResponseMessage(requestID);
            String content = response.getFileContent();

            // Store the content
            IDEManager ide = activator.getIDEManager();
            ide.storeContentForFileId(fileId, content);

            // Create a new ClientManager for this file
            ClientManager newClient = new ClientManager(fileId, response.getRevision());
            ide.storeClientForFileId(fileId, newClient);

        }
    }

    @Override
    public void beforeFileClosed(@NotNull FileEditorManager fileEditorManager, @NotNull VirtualFile virtualFile) {

    }
}

