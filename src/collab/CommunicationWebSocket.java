package collab;

import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.StatusCode;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;


/**
 * Created by Lisa on 8/09/2016. The abstract class that all socket classes
 * extend.
 */

@WebSocket(maxTextMessageSize = 64 * 1024)
public abstract class CommunicationWebSocket {

    private WebSocketClient client;
    protected String destination;

    private final CountDownLatch closeLatch;
    @SuppressWarnings("unused")
    private Session session;

    public CommunicationWebSocket() {
        this.closeLatch = new CountDownLatch(1);
    }

    public boolean awaitClose(int duration, TimeUnit unit) throws InterruptedException {
        return this.closeLatch.await(duration,unit);
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.printf("Connection closed: %d - %s%n",statusCode,reason);

        if (!client.isStopped()) {
           try {
               client.stop();
           } catch (Exception e) {
               e.printStackTrace();
           }
        }

        this.session = null;
        this.closeLatch.countDown(); // trigger latch
    }


    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.printf("Got connect: %s%n",session);
        this.session = session;
        session.setIdleTimeout(Long.MAX_VALUE);
    }


    /**
     * Connect to the websocket. Returns true if the connection is successful
     */
    public boolean connect() {

        try {
            client = new WebSocketClient();
            client.start();

            URI destinationURI = new URI(destination);
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            System.out.println("Destination: " + destination);
            client.connect(this, destinationURI, request);

        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }

        return true;
    }

    public boolean disconnect() {
        try {
            if (session.isOpen()) {
                session.close(StatusCode.NORMAL, "Close initiated");
                return true;
            } else {
                // no active session
                System.out.println("Session is not active");
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Sends the message asynchronously, as opposed to
     * blocking until the message has been fully sent
     * @param message - The message to send
     */
    public void sendMessage(String message) {
        session.getRemote().sendStringByFuture(message);
        System.out.println("Sending to node: " + message);
    }

    public boolean isConnected() {
        return session.isOpen();
    }


}
