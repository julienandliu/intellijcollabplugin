# Plugin for Multi User Collaboration

Setting Up
========================

## Author(s)
*   Romain Julien
*   Lisa Liu-Thorrold

--------------------------------------------------------------------------------
## Getting started
##### These instructions are for setup using the Intellij IDE 
1. Clone the repository from the following location

    https://lisa_liu-thorrold@bitbucket.org/julienandliu/intellijcollabplugin.git

2. Import the project into Intellij

--------------------------------------------------------------------------------

Completed features:

- Basic setup (top menu item, menu item when right clicking on the project on the left hand navigation bar)

-----------------------
To be implemented/coming soon...

- 

-----------------------